<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# ROGER Frames Components Library

## Development

Install dependencies.

```sh
npm run i-all
```

Initialize Husky.

```sh
npm run prepare
```

Start [dev server](http://localhost:3000).

```sh
npm run dev
```

<!-- currently, no test implemented (yet)
Run [tests](__tests__).

```sh
npm run test
```
-->

Test the production build of the component library (with profiling enabled).

```sh
npm run build-prod
npm run prod
```

## Framework Documentation Entrypoints

- [REACT](https://react.dev/)
- [MUI](https://mui.com/material-ui/getting-started/overview/)
- [JEST](https://jestjs.io/docs/tutorial-react)

## Contributing

This repo is [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) and uses [husky🐶](https://www.npmjs.com/package/husky) to ensure compliance with [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

## Distribution

Packages of the library are pushed to a [public npm registry](https://gitlab.gwdg.de/fe/npm) hosted by GWDG GitLab with the top-level scope "@fe". See the [README of the associated repository](https://gitlab.gwdg.de/fe/npm/-/blob/main/README.md) on how to install packages from this registry.

## License

This project aims to be [REUSE compliant](https://reuse.software/spec/).
Original parts are licensed under EUPL-1.2.
Derivative code and other assets are licensed under the respective license of the original.
Documentation, configuration and generated code files are licensed under CC0-1.0.

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/fe/roger/frames)](https://api.reuse.software/info/gitlab.gwdg.de/fe/roger/frames)
