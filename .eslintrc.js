// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

module.exports = {
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:import/react",
    "plugin:react/recommended",
    "plugin:security/recommended",
    "plugin:jsx-a11y/recommended",
    "plugin:import/typescript",
    "prettier",
  ],
  ignorePatterns: [".eslintrc.js", "**/dist/*.js", "**/node_modules", "/*.js"],
  plugins: [
    "react",
    "import",
    "jest",
    "jsx-a11y",
    "react-hooks",
    "@typescript-eslint",
  ],
  overrides: [
    {
      files: ["__tests__/**"],
      plugins: ["jest"],
      extends: ["plugin:jest/recommended"],
    },
  ],
  rules: {
    "react/prop-types": "off",
    "no-unused-vars": "warn",
    "react-hooks/rules-of-hooks": "error", // Checks rules of Hooks
    "react-hooks/exhaustive-deps": "warn", // Checks effect dependencies
    "security/detect-object-injection": "off",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
    project: true,
    tsconfigRootDir: __dirname,
  },
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  settings: {
    react: {
      version: "detect",
    },
    "import/parsers": {
      "@typescript-eslint/parser": [".js", ".jsx", ".ts", ".tsx"],
    },
  },
};
