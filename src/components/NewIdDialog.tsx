// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React from "react";
import {
  Button,
  ButtonGroup,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import { v4 as uuidv4 } from "uuid";
import { SelectShape, utils } from "@fe/roger-core";
import type { Quad, DataFactory } from "@rdfjs/types";
import type { Store } from "n3";

// Following is a hack to correctly type the default export of the imported module.
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-var-requires
const df: DataFactory = require("@rdfjs/data-model")

interface NewIdDialogProps {
  isOpen: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
  onCreate: [Quad[], React.Dispatch<React.SetStateAction<Quad[]>>];
  schema: Store;
  ns: string;
}

export default function NewIdDialog({
  isOpen: [open, setOpen],
  ns: idNamespace,
  onCreate: [dataQuads, setDataQuads],
  schema: schemaStore,
}: NewIdDialogProps) {
  /** User input. */
  const [input, setInput] = React.useState<string>("");
  /** User input error state. */
  const [inputError, setInputError] = React.useState<boolean>(false);
  /** Selected shape. */
  const [shape, setShape] = React.useState<string>("");

  /** Status of New Button */
  const newButtonDisabled = !shape || inputError;

  const inputValidationRegEx = /^[A-Za-z]([A-Za-z0-9.-])*$/;
  /** Helpertexts. */
  const helpText =
    "Input an identifier for your new Data. Leave empty for a random identifier to be created.";
  const errorText = `A valid identifier must start with a letter followed by letters, digits and the '.' and '-' characters only (${inputValidationRegEx.toString()}).`;
  /** Input validation on an intersection of url and xml:id allowed chars. */
  const validateInput = (input: string) => inputValidationRegEx.test(input);

  /** Set search term according to user input. */
  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.value && !validateInput(event.target.value)) {
      setInputError(true);
    } else {
      setInputError(false);
    }
    setInput(event.target.value);
  }

  /** Search on user pressing the enter key. */
  function handleKeyPress(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.key === "Enter") handleCreateAction();
  }

  function handleClickCancelButton() {
    setOpen(false);
  }

  function handleClickNewButton() {
    handleCreateAction();
  }

  function handleCreateAction() {
    // TODO: check for dataQuads: if length 0, insert; else ask for clear
    // currently, opening data is not yet implemented
    // if (dataQuads.length) ...

    // TODO: see implementation in Roger.js and factor out to utils, export it from roger-core
    const targetClass = schemaStore
      ?.getObjects(shape, "http://www.w3.org/ns/shacl#targetClass")
      .pop().value;

    // check for input length: if length 0 generate ID, else concat ns + input string
    const newQuad: Quad = df.quad(
      df.namedNode(`${idNamespace}${input ? input : uuidv4()}`),
      // TODO: let roger-core export IRIs
      df.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
      df.namedNode(targetClass)
    );
    setDataQuads([newQuad]);
    setOpen(false);
  }

  function handleSelectShape(shapeIri: string) {
    setShape(shapeIri);
  }

  return (
    <Dialog open={open}>
      <DialogTitle>Create object with new ID</DialogTitle>
      <DialogContent>
        <SelectShape
          shapes={utils.getNodeShapes(schemaStore)}
          selectedShape={utils.getShape(schemaStore, shape)}
          onSelectShape={handleSelectShape}
        />
        <TextField
          fullWidth
          error={inputError}
          helperText={inputError ? errorText : helpText}
          label="ID"
          onChange={handleInputChange}
          onKeyPress={handleKeyPress}
        />
      </DialogContent>
      <DialogActions>
        <ButtonGroup>
          <Button onClick={handleClickCancelButton}>Cancel</Button>
          <Button
            onClick={handleClickNewButton}
            variant="contained"
            disabled={newButtonDisabled}
          >
            New
          </Button>
        </ButtonGroup>
      </DialogActions>
    </Dialog>
  );
}
