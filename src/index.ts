// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

// fallback to SingleFrame as the default export of the library
export { default } from "./components/SingleFrame";
export { default as SingleFrame } from "./components/SingleFrame";
