// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import type { Quad } from "@rdfjs/types";

import * as RST from "rdf-string-ttl";
import axios from "axios";

export interface SparqlConfig {
  sparqlEndpoint: string;
  sparqlUpdateEndpoint: string;
}

/**
 * Update a resource, that is an array of Quads with the same subject, in a
 * triple store with SPARQL.
 *
 * @param quads
 */
export async function updateResource(quads: Quad[], config: SparqlConfig) {
  if (quads.length) {
    // convert the rdfjs typed quads to stringified quads
    const stringifiedQuads: RST.IStringQuad[] = [];
    for (const q of quads) stringifiedQuads.push(RST.quadToStringQuad(q));
    // build the query from the stringified quads
    let query = `DELETE { ${stringifiedQuads[0].subject} ?p ?o }\n
    INSERT {\n`;
    for (const sq of stringifiedQuads)
      query += `${sq.subject}  ${sq.predicate} ${sq.object} .\n`;
    query += `}\n
    WHERE { ?s ?p ?o }`;

    // execute the query against a sparql-update endpoint
    try {
      await axios.post(config.sparqlUpdateEndpoint, query, {
        headers: { "content-type": "application/sparql-update" },
      });
    } catch (error) {
      console.log(error); // TODO: handle messaging...
    }
  }
}
