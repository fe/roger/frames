// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

export const S = "subject";
export const P = "predicate";
export const O = "object";
export const T = "text";
export const R = "result";
export const AR = "aggregatedResults";
